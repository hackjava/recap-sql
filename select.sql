-- select *
-- from fornitori;
-- select p.nome, f.nome
-- from fornitori f
-- left join fornitori_prodotti fp on f.id = fp.fornitore_id
-- left join prodotti p on fp.prodotto_id = p.id
-- where f.id = 3;
-- contiene
-- p.nome LIKE '%pixel%';
-- alla fine
-- p.nome LIKE '%pixel';
-- all'inizio
-- p.nome LIKE 'pixel%';

select p.nome, p.prezzo, f.nome
from prodotti p
join fornitori_prodotti fp on p.id = fp.prodotto_id
join fornitori f on fp.fornitore_id = f.id
where p.prezzo <= 900;
